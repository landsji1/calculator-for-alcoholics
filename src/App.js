import './App.css';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/pages/Home'
import Settings from './components/pages/Settings'
import About from './components/pages/About';


function App() {

  localStorage.setItem("gender", 'male');
  localStorage.setItem("weight", 75);
  localStorage.setItem("genderCoefficientValue", '1.66666666667');
  localStorage.setItem('beer', 0);
  localStorage.setItem('shot', 0);
  localStorage.setItem('wine', 0);

  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/settings' exact component={Settings} />
          <Route path='/about' exact component={About} />
        </Switch>
      </Router>
    </>
  );
}



export default App;
