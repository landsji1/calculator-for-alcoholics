import React from 'react'
import'./AboutSection.css';
import '../App.css'

function AboutSection() {
    return (
        <div className='about-container'>
            <br></br>
            <h2>This app is for drunk people who want to know when are they gonna be sober.</h2>
            <br></br>
            <p>Made by drunk people for drunk people</p>
            <br></br>
            <p>You gonna need to fill the data in settings for this app to work correctly.</p>
            <br></br>
            <p>Cheers!</p>
            <br></br>
            <img src="images/cheers.png" alt="Cheers" width="350" height="400"></img>
        </div>
    )
}

export default AboutSection
