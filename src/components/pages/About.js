import React from 'react'
import AboutSection from '../AboutSection'

function About() {
    return (
        <div className='about-'>
            <AboutSection/>
        </div>
    )
}

export default About
