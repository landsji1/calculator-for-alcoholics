
import React, { Component } from 'react'
import '../../App.css'
import Results from '../Results'
import './Home.css'


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            weight: ''
        }
    }

    addBeer = (event) => {
        var amount =  parseInt(localStorage.getItem('beer')) + 1
        localStorage.setItem('beer', amount);
        console.log(localStorage.getItem('beer'));
        document.getElementById('numberOfBeers').innerHTML = "Number of beers:" + amount;
        this.forceUpdate()
    }

    removeBeer = (event) => {
        if(parseInt(localStorage.getItem('beer')) - 1 < 0) {
            alert("Number can not be lower than 0!")
        } else {
            var amount =  parseInt(localStorage.getItem('beer')) - 1
            localStorage.setItem('beer', amount);
            console.log(localStorage.getItem('beer'));
            document.getElementById('numberOfBeers').innerHTML = "Number of beers:" + amount;
            this.forceUpdate()
        }
    }

    addShot = (event) => {
        var amount =  parseInt(localStorage.getItem('shot')) + 1
        localStorage.setItem('shot', amount);
        console.log(localStorage.getItem('shot'));
        document.getElementById('numberOfShots').innerHTML = "Number of shots:" + amount;
        this.forceUpdate()
    }

    removeShot = (event) => {
        if(parseInt(localStorage.getItem('shot')) - 1 < 0) {
            alert("Number can not be lower than 0!")
        } else {
            var amount =  parseInt(localStorage.getItem('shot')) - 1
            localStorage.setItem('shot', amount);
            console.log(localStorage.getItem('shot'));
            document.getElementById('numberOfShots').innerHTML = "Number of shots:" + amount;
            this.forceUpdate()
        }
    }

    addWine = (event) => {
        var amount =  parseInt(localStorage.getItem('wine')) + 1
        localStorage.setItem('wine', amount);
        console.log(localStorage.getItem('wine'));
        document.getElementById('numberOfWines').innerHTML = "Number of wines:" + amount;
        this.forceUpdate()
    }

    removeWine = (event) => {
        if(parseInt(localStorage.getItem('wine')) - 1 < 0) {
            alert("Number can not be lower than 0!")
        } else {
            var amount =  parseInt(localStorage.getItem('wine')) - 1
            localStorage.setItem('wine', amount);
            console.log(localStorage.getItem('wine'));
            document.getElementById('numberOfWines').innerHTML = "Number of wines:" + amount;
            this.forceUpdate()
        }
    }

    



    render() {
        return (
            <div className='buttons'>
                <div id='beers' className='beers'>
                    <span id='numberOfBeers' class="badge badge-warning">Number of beers:0</span>
                    <br /><br />
                    <button id='beer' onClick={this.addBeer} class="btn btn-primary">ADD BEER</button> 
                    &nbsp;&nbsp;&nbsp;
                    <button id='beerRemove' onClick={this.removeBeer} class="btn btn-primary">REMOVE BEER</button>
                    
                </div>
                <div id='shots' className='shots'>
                    <span id='numberOfShots' class="badge badge-warning">Number of shots:0</span>
                    <br /><br />
                    <button id='shot' onClick={this.addShot} class="btn btn-primary">ADD SHOT</button> 
                    &nbsp;&nbsp;&nbsp;
                    <button id='shotRemove' onClick={this.removeShot} class="btn btn-primary">REMOVE SHOT</button>
                    
                </div>
                <div id='wines' className='wines'>
                    <span id='numberOfWines' class="badge badge-warning">Number of wines:0</span>
                    <br /><br />
                    <button id='wine' onClick={this.addWine} class="btn btn-primary">ADD WINE</button> 
                    &nbsp;&nbsp;&nbsp;
                    <button id='wineRemove' onClick={this.removeWine} class="btn btn-primary">REMOVE WINE</button>
                    
                </div>
                <Results/>
            </div>
        );
    }
}

export default Home;