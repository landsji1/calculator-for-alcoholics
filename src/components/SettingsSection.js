import './SettingsSection.css';
import '../App.css'
import React, { Component } from 'react'


class SettingsSection extends Component {

    constructor(props){
        super(props);
        this.state = {
          weight : ''
        }
    }

    weightChangeHandler = (event) => {
        event.preventDefault()
        this.setState({ weight: event.target.value });
        var weight = event.target.value;
        localStorage.setItem("weight", weight);
        console.log(localStorage.getItem('weight'));
    }

    genderChangeHandler = (event) => {
        this.setState({ weight: event.target.value });
        var gender = event.target.value;
        localStorage.setItem("gender", gender);
        if(gender==='male') localStorage.setItem("genderCoefficientValue", '1.66666666667');
        if(gender==='female') localStorage.setItem("genderCoefficientValue", '1.42857142857');
        console.log(localStorage.getItem('gender'));
    }

    render() {
        return (

            <div className='settings-container'>
                <h1>Don't want to be rude, but I need some info about you.</h1>

                <br /><h3>What is your sex?</h3><br />

            
                  <select name="gender" id="gender" value={localStorage.getItem('gender')} onChange={this.genderChangeHandler}>
                    <option value="male">male</option>
                    <option value="female">female</option>
                  </select>
                
            

                <form id='weightInsert'>
                    <br /><h3>What is your weight?</h3><br />
                    <input id='weight' type="number" aria-label="your weight" value={localStorage.getItem('weight')} onChange={this.weightChangeHandler}  min='35' max='200'></input>
                    <br />
                </form >


            </div>
        )
    }
};


export default SettingsSection
