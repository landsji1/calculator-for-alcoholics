
import './Results.css'

function Results() {
    
    const soberingCoefficient = 6.66666666667;

	// Math.ceil(ethanolDensity * this.number * this.shot * this.proof / 100);

	function BAC() {
		var ethanolMass = (parseInt(localStorage.getItem('beer')) * 0.055 * 500) * 0.789  + (parseInt(localStorage.getItem('wine'))*0.116*200)*0.789 + (parseInt(localStorage.getItem('shot'))*0.40*40)*0.789
		console.log('weight ' + parseInt(localStorage.getItem('weight')));
		console.log('genderCoeff ' + parseInt(localStorage.getItem('genderCoefficientValue')));
		console.log('ethanolmass ' + ethanolMass)
		return parseInt(localStorage.getItem('genderCoefficientValue')) * ethanolMass / parseInt(localStorage.getItem('weight'));
	}
	function soberingTime(bac) {
		console.log(bac);
		return Math.ceil(bac * soberingCoefficient);
	}

    
    return (
		
		<div className='result-container'>

        	<output className="row">
				<p className="h3">Your blood alcohol level (&permil;):</p>
				<span className="h3">{(BAC()).toFixed(1)}</span>		
				<br></br>
				<p className="h3">You'll be sober again in, approximately:</p>
				<span className="h3">{soberingTime(BAC())} hours</span>		

			</output>
		</div>
    );   
}

export default Results